class BooksController < ApplicationController
  before_action :authenticate_user!, except: [:index, :show]

  def index
    @books = case params[:sort]
              when 'All'
                Book.all
               when 'Satire'
                 Genre.where(title: 'Satire').first.books
              when 'Health'
                Genre.where(title: 'Health').first.books
              when 'Guide'
                Genre.where(title: 'Guide').first.books
              when 'Travel'
                Genre.where(title: 'Travel').first.books
              when 'Science'
                Genre.where(title: 'Science').first.books
              else
                Book.all
            end
  end

  def show
    @book = Book.find(params[:id])
  end

  def new
    @book = Book.new
    @book_images = @book.book_images.build
  end

  def create
    @user = current_user
    @book = @user.books.new(book_params)

    if params[:agree].blank?
      redirect_back(fallback_location: root_path, notice: 'You must be agree.')
    elsif @book.save
      params[:book][:genres].each do |genre|
        BookGenre.create(book: @book, genre_id: genre.to_i) unless genre.blank?
      end

      params[:book][:authors].each do |author|
        AuthorBook.create(book: @book, author_id: author.to_i) unless author.blank?
      end

      params[:book][:image].each do |image|
        @book_images = @book.book_images.create!(image: image)
      end

      redirect_to root_path, notice: 'Book was created, but the book needs approval.'
    else
      redirect_to root_path, notice: 'Something wrong.'
    end
  end

  private

  def book_params
    params.require(:book).permit(:title, :description, book_attachments_attributes: [:id, :book_id, :image])
  end
end

class AuthorsController < ApplicationController
  before_action :authenticate_user!

  def new
    @author = Author.new
  end

  def create
    @author = Author.new(author_params)

    if params[:agree].blank?
      redirect_back(fallback_location: root_path, notice: 'You must be agree.')
    elsif @author.save
      redirect_to root_path, notice: 'Author was created.'
    else
      redirect_to root_path, notice: 'Something wrong.'
    end
  end

  private

  def author_params
    params.require(:author).permit(:name_surname)
  end
end

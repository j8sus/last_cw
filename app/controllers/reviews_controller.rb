class ReviewsController < ApplicationController
  before_action :authenticate_user!

  def create
    @review = current_user.reviews.new(review_params)
    @book = Book.find(params[:review][:book_id])

    if @review.save
      @book.update_rating
      redirect_back(fallback_location: root_path, notice: 'Review was created.')
    else
      redirect_back(fallback_location: root_path, notice: 'Something wrong.')
    end
  end

  def destroy
    @review = Review.find(params[:id])

    if @review.user == current_user
      @review.destroy
      flash[:notice] = "Post was destroyed."
      redirect_back(fallback_location: root_path)
    else
      flash[:notice] = "No access."
      redirect_back(fallback_location: root_path)
    end
  end

  def review_params
    params.require(:review).permit(:comment, :rate, :book_id)
  end
end

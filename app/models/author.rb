class Author < ApplicationRecord
  attr_accessor :checkbox

  has_many :author_books
  has_many :books, through: :author_books
end

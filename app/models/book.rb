class Book < ApplicationRecord
  attr_accessor :checkbox

  belongs_to :user

  has_many :book_genres
  has_many :genres, through: :book_genres

  has_many :author_books
  has_many :authors, through: :author_books

  has_many :book_images
  accepts_nested_attributes_for :book_images

  has_many :reviews

  def update_rating
    self.rating =  self.reviews.average(:rate)
    self.save
  end

  def author_rating
    self.reviews.where(user: self.user).first.rate unless self.reviews.where(user: self.user).blank?
  end

  def self.search(pattern)
    if pattern.blank?  # blank? covers both nil and empty string
      all
    else
      where('name LIKE ?', "%#{pattern}%")
    end
  end
end

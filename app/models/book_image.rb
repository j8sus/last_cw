class BookImage < ApplicationRecord
  mount_uploader :image, ImageUploader
  belongs_to :book
end

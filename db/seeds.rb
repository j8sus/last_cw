genres = []

Genre::GENRE_SAMPLE.each do |genre|
  genres << Genre.create!(title: genre)
end

authors = []

authors << Author.create!(name_surname: 'Ernest Hemingway')
authors << Author.create!(name_surname: 'Maxim Gorky')
authors << Author.create!(name_surname: 'Nikolai Gogol')

3.times do |i|
  user = User.create!(name: 'Pavel', surname: 'Durov', email: 'mail' + i.to_s + '@gmail.com', password: 'qweqwe')
  book = Book.create!(user: user, title: 'Harry Potter', description: 'Good Book', active: true)

  BookGenre.create!(book: book, genre: genres[i])
  AuthorBook.create!(book: book, author: authors[i])
end

